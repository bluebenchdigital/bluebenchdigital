import React from "react";
import "./assets/styles/_headerStyles.scss";

export const Header = () => {
  return (
    <div className="header">
      <nav>
        <ul className="navMain">
          <li className="navLink">
            <a href="#contact">Contact</a>
          </li>
        </ul>

        <ul className="navSocial">
          <li className="socialLink">
            <a
              href="https://www.facebook.com/bluebenchdave"
              target="_blank"
              rel="noopener noreferrer"
            >
              Facebook
            </a>
          </li>
          <li className="socialLink">
            <a
              href="https://www.instagram.com/bluebench_dave/"
              target="_blank"
              rel="noopener noreferrer"
            >
              Instagram
            </a>
          </li>
        </ul>
      </nav>
    </div>
    
  );
};
