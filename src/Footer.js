import React from "react";

export const Footer = () => {
  return (
    <div>
      <div className="iconRow">
        <div>
          <h4>Trusted Relationships</h4>
        </div>
      </div>

      <div className="mainFooter">
        <div>
          <a href="tel:14147310441">414.731.0441</a>
        </div>
        <div>
          <button>Shoot me an email</button>
        </div>
      </div>

      <div className="subFooter">
        <h5>
          &copy; {new Date().getFullYear()} Blue Bench | All rights reserved
        </h5>
      </div>
    </div>
  );
};
