import React from "react";

export const Hero = () => {
  return (
    <div className="hero">
      <h1>Blue Bench Web Consultant</h1>
    </div>
  );
};
