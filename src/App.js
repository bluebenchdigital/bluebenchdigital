import React from "react";
import "./assets/styles/main.scss";
import { Header } from "./Header";
import { Hero } from "./Hero";
import { Services } from "./Services";
import { Cases } from "./Cases";
import { Testimonials } from "./Testimonials";
import { Footer } from "./Footer";

export default function App() {
  return (
    <div className="App">
      <Header />
      <Hero />
      <Services />
      <Cases />

      <div className="aboutDave">
        <h2>Hi! I'm Dave</h2>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis non
          purus porttitor, mollis libero ut, tincidunt velit. Aenean at
          pellentesque leo, vel rutrum lectus. Maecenas pharetra sit amet sem at
          eleifend. Donec finibus nec magna at semper. Duis iaculis pulvinar
          lorem. Sed pretium, nibh sit amet dictum euismod, ex
        </p>
        <button>Shoot me an email</button>
      </div>

      <Testimonials />
      <Footer />
    </div>
  );
}
